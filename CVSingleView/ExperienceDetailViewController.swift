//
//  ExperienceDetailViewController.swift
//  CVSingleView
//
//  Created by Conrad Tingström 2019-11-12.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var experienceDetailImage: UIImageView!
    @IBOutlet weak var experienceDetailName: UILabel!
    @IBOutlet weak var experienceDetailPeriod: UILabel!
    @IBOutlet weak var experienceDetailDescription: UILabel!
    
    var experienceImage: UIImage?
    var experienceName: String = ""
    var experiencePeriod: String = ""
    var experienceDescription: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = experienceName
        self.experienceDetailImage.image = experienceImage
        self.experienceDetailName.text = experienceName
        self.experienceDetailPeriod.text = experiencePeriod
        self.experienceDetailDescription.text = experienceDescription
    }

}
