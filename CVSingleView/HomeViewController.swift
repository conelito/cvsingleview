//
//  HomeViewController.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-04.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var contactPhone: UILabel!
    @IBOutlet weak var contactAddress: UILabel!
    @IBOutlet weak var contactCity: UILabel!
    @IBOutlet weak var presentation: UILabel!
    @IBOutlet weak var homeImage: UIImageView!
    
    let homeContent: HomeContent = HomeContent()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle.title = homeContent.name
        contactPhone.text = homeContent.phone
        contactAddress.text = homeContent.address
        contactCity.text = homeContent.city
        presentation.text = homeContent.presentation
        homeImage.image = UIImage(named: "HomeImage")
    }
    
}
 
