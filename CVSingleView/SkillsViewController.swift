//
//  SkillsViewController.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-12.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var arrowImage: UIImageView!
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @IBAction func dismissButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.rotateView(targetView: self.arrowImage)
    }
    
    func rotateView(targetView: UIView, duration: Double = 1) {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            targetView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }, completion: nil)

        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveLinear, animations: {
            targetView.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2.0)
        }) { finished in
            self.rotateView(targetView: targetView)
        }
    }

}
