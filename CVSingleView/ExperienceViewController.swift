//
//  ExperienceViewController.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-04.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {
    
    @IBOutlet weak var experienceTableView: UITableView!
    
    let sectionsInTableView = ["Work", "Education"]
    var experiences = [
        [
            Experience(image: UIImage.self(named: "WorkDefault"), name: "Work 1", period: "2013-2014"),
            Experience(image: UIImage.self(named: "WorkDefault"), name: "Work 2", period: "2016-current")
        ],
        [
            Experience(image: UIImage.self(named: "EducationDefault"), name: "Education 1", period: "2009-2012"),
            Experience(image: UIImage.self(named: "EducationDefault"), name: "Education 2", period: "2012-2013"),
            Experience(image: UIImage.self(named: "EducationDefault"), name: "Education 3", period: "2018-current")
        ]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        experienceTableView.delegate = self
        experienceTableView.dataSource = self
    }
    
}
 
extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionsInTableView.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionsInTableView[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell", for:indexPath) as? ExperienceTableViewCell{
            cell.experienceImage?.image = self.experiences[indexPath.section][indexPath.row].image;
            cell.experienceName.text = self.experiences[indexPath.section][indexPath.row].name;
            cell.experiencePeriod.text = self.experiences[indexPath.section][indexPath.row].period;
            return cell;
        }
        return UITableViewCell();
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let experienceInformation: Experience? = experiences[indexPath.section][indexPath.row]
        performSegue(withIdentifier: "ExperienceDetailSegue", sender: experienceInformation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueDestination = segue.destination as? ExperienceDetailViewController
        let experienceInformation: Experience? = sender as? Experience
        segueDestination?.experienceImage = experienceInformation?.image
        segueDestination?.experienceName = experienceInformation?.name ?? "Failed to load :("
        segueDestination?.experiencePeriod = experienceInformation?.period ?? "Failed to load :("
        segueDestination?.experienceDescription = experienceInformation?.getDescription() ?? "Failed to load :("
    }
    
}
