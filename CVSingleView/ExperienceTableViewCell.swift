//
//  ExperienceTableViewCell.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-09.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {

    @IBOutlet weak var experienceImage: UIImageView!
    @IBOutlet weak var experienceName: UILabel!
    @IBOutlet weak var experiencePeriod: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
