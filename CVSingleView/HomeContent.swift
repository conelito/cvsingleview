//
//  HomeContent.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-08.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import Foundation

struct HomeContent {
    
    var name: String = "Conrad Tingström"
    var phone: String = "0762811640"
    var address: String  = "Linnégatan 14A"
    var city: String = "Huskvarna"
    var presentation: String = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

    Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
    """
    
}
