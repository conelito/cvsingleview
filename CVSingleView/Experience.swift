//
//  Experience.swift
//  CVSingleView
//
//  Created by Conrad Tingström on 2019-11-09.
//  Copyright © 2019 Conrad Tingström. All rights reserved.
//

import Foundation
import UIKit

class Experience {
    
    var image: UIImage?
    var name: String
    var period: String
    
    init(image: UIImage?, name: String, period: String) {
        if image != nil {
            self.image = image
        }
        self.name = name
        self.period = period
    }
    
    func getDescription() -> String {
        switch self.name {
        case "Work 1":
            return """
            Description of work 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

            Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
            """
        case "Work 2":
            return """
            Description of work 2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

            Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
            """
        case "Education 1":
            return """
             Description of education 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

             Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
            """
        case "Education 2":
            return """
            Description of education 2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

            Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
            """
         case "Education 3":
            return """
            Description of education 3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut lobortis mauris. Nunc vel nibh erat. Quisque volutpat felis vel libero ullamcorper dapibus. Donec semper, ex a interdum elementum, dui dui tempus purus, a egestas purus ipsum sed lectus. Phasellus id elit mi. Ut ut tempor erat. Fusce suscipit venenatis eros.

            Cras ligula mauris, laoreet at sapien sed, volutpat dictum massa. Nulla vitae laoreet ex. Donec vel mauris ante. Quisque maximus ante rhoncus finibus blandit. Nam nisi ligula, pellentesque id nisl non, consequat facilisis purus. Mauris in dolor iaculis, interdum urna et, euismod dui. Aliquam fringilla ut nunc vitae porta. Vestibulum et aliquam justo. Quisque consequat justo sapien, a lacinia velit vestibulum sed. Nullam vestibulum mi blandit turpis convallis vehicula.
            """
        default:
            return "No description."
        }
    }
    
}
